#include "rectangle.h"

RectangleShape::RectangleShape(const QPointF &pt1, const QPointF &pt2)
{
    nodes.reserve(8);
    for(int i = 0; i < 8; i++)
        nodes.push_back(QPointF(0,0));
    setRect(mapFromScene(pt1), mapFromScene(pt2));
    capturedNode = 4;
}


RectangleShape::RectangleShape(QDataStream &in): NodesShape(in)
{
    pathsChanged();
}


bool RectangleShape::changeNode(int i, const QPointF &ptTo)
{
    if (i < 0 || i >= nodes.count())
        return false;

    static bool equalPt1X[] = {true, true, true, false, false, false, false, false};
    static bool equalPt1Y[] = {true, false, false, false, false, false, true, true};
    static bool equalPt2X[] = {false, false, false, false, true, true, true, false};
    static bool equalPt2Y[] = {false, false, true, true, true, false, false, false};

    QPointF pt = mapFromScene(ptTo);
    QPointF pt1 = nodes[0], pt2 = nodes[4];

    if (equalPt1X[i]) {
        pt1.rx() = pt.x();
        if ((pt2.x() - pt1.x()) < getMinSideLen())
            pt1.rx() = pt2.x() - getMinSideLen();
    }
    if (equalPt1Y[i]) {
        pt1.ry() = pt.y();
        if ((pt2.y() - pt1.y()) < getMinSideLen())
            pt1.ry() = pt2.y() - getMinSideLen();
    }
    if (equalPt2X[i]) {
        pt2.rx() = pt.x();
        if ((pt2.x() - pt1.x()) < getMinSideLen())
            pt2.rx() = pt1.x() + getMinSideLen();
    }
    if (equalPt2Y[i]) {
        pt2.ry() = pt.y();
        if ((pt2.y() - pt1.y()) < getMinSideLen())
            pt2.ry() = pt1.ry() + getMinSideLen();
    }

    setRect(pt1, pt2);
    return true;
}


void RectangleShape::setRect(const QPointF &pt1s, const QPointF &pt2s)
{
    qreal w_2 = (pt2s.x() - pt1s.x()) / 2;
    qreal h_2 = (pt2s.y() - pt1s.y()) / 2;
    QPointF originPos(pt1s.x() + w_2, pt1s.y() + h_2);
    setPos(mapToScene(originPos));
    QPointF pt1 = pt1s - originPos;
    QPointF pt2 = pt2s - originPos;

    nodes[0] = pt1;
    nodes[1] = QPointF(pt1.x(), pt1.y() + h_2);
    nodes[2] = QPointF(pt1.x(), pt2.y());
    nodes[3] = QPointF(pt1.x() + w_2, pt2.y());
    nodes[4] = pt2;
    nodes[5] = QPointF(pt2.x(), pt1.y() + h_2);
    nodes[6] = QPointF(pt2.x(), pt1.y());
    nodes[7] = QPointF(pt1.x() + w_2, pt1.y());
    setTransformOriginPoint(pt1.x() + w_2, pt1.y() + h_2);
    updatePaths();
}


void RectangleShape::pathsChanged()
{
    outlinePath.closeSubpath();
    updateGradientBrush();
}


