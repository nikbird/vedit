#ifndef RECTANGLE_H
#define RECTANGLE_H

#include "shape.h"
#include <QDataStream>


class RectangleShape: public NodesShape
{
public:
    RectangleShape(const QPointF& pt1, const QPointF& pt2);
    RectangleShape(QDataStream& in);

    virtual bool changeNode(int i, const QPointF& pt);
    virtual bool addNode(const QPointF& pt) { return true; pt; }

protected:
    void setRect(const QPointF& pt1, const QPointF& pt2);
    virtual void pathsChanged();

};



#endif // RECTANGLE_H
