#include "shape.h"
#include <exception>

NodesShape::NodesShape(): capturedNode(-1)
{
    lineWidthChanged();
    nodesPen = QPen(QBrush(Qt::blue), 1);
    nodesBrush = QBrush(Qt::blue);
    setFlags(ItemIsSelectable | ItemIsMovable);
}


NodesShape::NodesShape(QDataStream &in): BaseShape(in), capturedNode(-1)
{
    qreal rot;
    in >> nodes >> nodeRadius >> nodesPen >> nodesBrush >> rot;
    lineWidthChanged();
    nodesPen = QPen(QBrush(Qt::blue), 1);
    nodesBrush = QBrush(Qt::blue);
    setFlags(ItemIsSelectable | ItemIsMovable);
    setRotation(rot);
}


QList<int> NodesShape::nodesAroundPoint(const QPointF& pt) const
{
    QList<int> finded;
    for(int i = 0; i < nodes.count(); i++) {
        if (pointInNode(pt, i)) {
            finded.push_back(i);
        }
    }
    return finded;
}

void NodesShape::serialize(QDataStream &out)
{
    BaseShape::serialize(out);
    out << nodes << nodeRadius << nodesPen << nodesBrush << rotation();
}


bool NodesShape::pointInNode(const QPointF &pt, int i) const
{
    return (qAbs(pt.x() - nodes[i].x()) < nodeRadius && qAbs(pt.y() - nodes[i].y()) < nodeRadius);
}


void NodesShape::updatePaths()
{
    if (nodes.count() < 2)
        return;

    prepareGeometryChange();

    outlinePath = QPainterPath(nodes[0]);
    outlinePath.setFillRule(Qt::WindingFill);
    nodesPath = QPainterPath();
    nodesPath.setFillRule(Qt::WindingFill);
    nodesPath.addEllipse(nodes[0], nodeRadius, nodeRadius);
    for(int i = 1; i < nodes.count(); i++) {
        outlinePath.lineTo(nodes[i]);
        nodesPath.addEllipse(nodes[i], nodeRadius, nodeRadius);
    }
    pathsChanged();
}


void NodesShape::lineWidthChanged()
{
    nodeRadius = getLineWidth() * 0.65 + 2;
    updatePaths();
}


QRectF NodesShape::boundingRect() const
{
    qreal adj = nodesPen.widthF() / 2;
    return nodesPath.boundingRect().adjusted(-adj, -adj, adj, adj);
}

void NodesShape::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->setBrush(getBrush());
    painter->setPen(getPen());
    painter->drawPath(outlinePath);
    if (isSelected()) {
        painter->setBrush(nodesBrush);
        painter->setPen(nodesPen);
        painter->drawPath(nodesPath);
    }
    option, widget;
}


void NodesShape::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    if (nodesPath.contains(event->pos())) {
        QPointF& pt = event->pos();
        for(int i = 0; i < nodes.count(); i++) {
            if (pointInNode(pt, i)) {
                capturedNode = i;
                break;
            }
        }
    }
    BaseShape::mousePressEvent(event);
}


void NodesShape::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    capturedNode = -1;
    BaseShape::mouseReleaseEvent(event);
}


void NodesShape::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    if (capturedNode == -1)
        BaseShape::mouseMoveEvent(event);
    else
        changeNode(capturedNode, event->scenePos());
}


