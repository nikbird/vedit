#include "mygraphicsview.h"

#include <QMouseEvent>


ActionMode curActionMode;
ShapeCreatorFunc shapeCreate;


MyGraphicsView::MyGraphicsView()
{
    setScene(new QGraphicsScene());
}

void MyGraphicsView::moveWorkArea(int dx, int dy)
{
    QRectF rect = sceneRect();
    rect.adjust(-dx, -dy, -dx, -dy);
    setSceneRect(rect);
}

void MyGraphicsView::moveWorkArea(const QPoint &pt)
{
    QRectF rect = sceneRect();
    rect.adjust(-pt.x(), -pt.y(), -pt.x(), -pt.y());
    setSceneRect(rect);
}


void MyGraphicsView::resizeEvent(QResizeEvent *event)
{
    QSize s = event->size();
    setSceneRect(sceneRect().x(), sceneRect().y(), s.width(), s.height());
}


void MyGraphicsView::mousePressEvent(QMouseEvent *event)
{
    pressPos = event->pos();
    prevMousePos = pressPos;
    switch (curActionMode) {
    case AMView:
        setDragMode(ScrollHandDrag);
        break;
    case AMCreation:
        capturedShape = shapeCreate(mapToScene(pressPos), mapToScene(pressPos));
        capturedShape->setSelected(true);
        scene()->addItem(capturedShape);
        break;
    case AMDeletion:
        if (auto item = itemAt(pressPos)) {
            delete item;
            deletedSingleItem = true;
        }
        else if (event->modifiers() == Qt::ShiftModifier) {
            setDragMode(RubberBandDrag);
            QGraphicsView::mousePressEvent(event);
            deletedSingleItem = false;
        }
        break;
    case AMChange:
        if (!itemAt(pressPos)) {
            if (event->modifiers() == Qt::ShiftModifier) {
                setDragMode(RubberBandDrag);
            }
            shapeCaptured = false;
        }
        else {
            shapeCaptured = true;
        }
        QGraphicsView::mousePressEvent(event);
        break;
    case AMRotation:
        scene()->clearSelection();
        capturedShape = static_cast<NodesShape*>(itemAt(pressPos));
        if (capturedShape) {
            shapeCaptured = true;
            capturedShape->setSelected(true);
            curRotation = capturedShape->rotation();
        }
        else {
            shapeCaptured = false;
        }
        break;
    default:
        break;
    }
}


void MyGraphicsView::mouseMoveEvent(QMouseEvent * event)
{
    switch (curActionMode) {
    case AMView:
        moveWorkArea(event->pos() - prevMousePos);
        break;
    case AMCreation:
        if (capturedShape) {
            capturedShape->changeCapturedNode(mapToScene(event->pos()));
        }
        break;
    case AMDeletion:
        QGraphicsView::mouseMoveEvent(event);
        break;
    case AMChange:
        QGraphicsView::mouseMoveEvent(event);
        break;
    case AMRotation:
        if (capturedShape) {
            capturedShape->setRotation(curRotation + pressPos.x() - event->pos().x());
        }
        break;
    default:
        break;
    }
    prevMousePos = event->pos();
}


void MyGraphicsView::mouseReleaseEvent(QMouseEvent *event)
{
    switch (curActionMode) {
    case AMView:
        setDragMode(NoDrag);
        break;
    case AMCreation:
        if (capturedShape) {
           capturedShape->clearCapturedNode();
           capturedShape = 0;
        }
        //scene()->addItem(shapeCreate(mapToScene(event->pos()), mapToScene(pressPos)));
        break;
    case AMDeletion:
        if (!deletedSingleItem) {
            QGraphicsView::mouseReleaseEvent(event);
            setDragMode(NoDrag);
            for(int i = 0, count = scene()->selectedItems().count(); i < count; i++) {
                delete scene()->selectedItems()[0];
            }
        }
        break;
    case AMChange:
        QGraphicsView::mouseReleaseEvent(event);
        if (scene()->selectedItems().count() > 1) {
            box1ForDisable->setDisabled(true);
            box2ForDisable->setDisabled(true);
        }
        else {
            box1ForDisable->setDisabled(false);
            box2ForDisable->setDisabled(false);
        }
        if (!shapeCaptured) {
            setDragMode(NoDrag);
        }
        break;
    case AMRotation:
        break;
    default:
        break;
    }
}

void MyGraphicsView::mouseDoubleClickEvent(QMouseEvent *event)
{
    if (curActionMode == AMChange)
        QGraphicsView::mouseDoubleClickEvent(event);
}

void MyGraphicsView::keyPressEvent(QKeyEvent *event)
{
    if ((curActionMode == AMCreation) && (capturedShape)) {
        if (event->key() == Qt::Key_Space)
            capturedShape->addNode(mapToScene(prevMousePos));
    }
}

