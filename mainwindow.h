#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "mygraphicsview.h"
#include "rectangle.h"
#include "polyline.h"


namespace Ui {
class MainWindow;
}

extern NodesShape* createRectShape(const QPointF &pt1, const QPointF &pt2);
extern NodesShape *createPolyShape(const QPointF &pt1, const QPointF &pt2);


class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void saveSceneToFile();
    void loadSceneFromFile();
    void clearScene();

    void btnShapeRectToggled();
    void btnShapePolyToggled();

    void useLineParams();
    void sboxLineWidthChanged(int);

    void useFilling();
    void btnNoFillChecked();
    void btnSolidFillChecked();
    void btnGradientFillChecked();

    void btnAMViewToggled();
    void btnAMCreationToggled();
    void btnAMDeletionToggled();
    void btnAMChangeToggled();
    void btnAMRotationToggled();

    void setLineColor();
    void setColor1();
    void setColor2();

private:
    Ui::MainWindow *ui;
    MyGraphicsView* gView;
};

#endif // MAINWINDOW_H
