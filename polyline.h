#ifndef POLYLINE_H
#define POLYLINE_H

#include "shape.h"
#include <QDataStream>


class PolyShape: public NodesShape
{
public:
    PolyShape(const QPointF& pt1, const QPointF& pt2);    // +
    PolyShape(QDataStream& in);

    virtual bool changeNode(int i, const QPointF& pt); //+
    virtual bool addNode(const QPointF& pt);

    virtual void pathsChanged();    // +
    virtual void lineWidthChanged();    // +

    QPainterPath shape() const { return segPath + nodesPath; }  // +
    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

protected:
    void updateSegments();  //+
    void addSegment(QPainterPath &path, int endIndex); //+

    void mouseReleaseEvent(QGraphicsSceneMouseEvent * event);
    void mouseDoubleClickEvent(QGraphicsSceneMouseEvent * event);

private:
    QList<QPainterPath> segments;
    QPainterPath segPath;
};



#endif // POLYLINE_H
