#ifndef BASESHAPE_H
#define BASESHAPE_H

#include <QPen>
#include <QGraphicsItem>
#include <QPainterPath>
#include <QDataStream>

class BaseShape : public QGraphicsItem
{
public:
    static const int minLineWidth = 1;
    enum FillStyle { FSNoFill = Qt::NoBrush, FSSolid = Qt::SolidPattern, FSGradient = Qt::LinearGradientPattern};

public:
    BaseShape(qreal w, const QColor& lcol, FillStyle fs, const QColor& c1, const QColor& c2);
    BaseShape(QDataStream& in);
    BaseShape();

    qreal getLineWidth() const { return lineWidth; }
    const QColor& getLineColor() const { return lineColor; }
    FillStyle getFillStyle() const { return fillStyle; }
    const QColor& getSolidColor() const { return solidColor; }
    const QColor& getGradientColor1() const { return gColor1; }
    const QColor& getGradientColor2() const { return gColor2; }
    const void getGradientColors(QColor& c1, QColor c2) const { c1 = gColor1; c2 = gColor2; }
    const QPen& getPen() const { return curPen; }
    const QBrush& getBrush() const { return *curBrush; }

    qreal setLineWidth(qreal w);
    void setLineColor(const QColor& c) { lineColor = c; curPen.setColor(c); }
    void setFillStyle(FillStyle fs);
    void setSolidColor(const QColor& c) { solidColor = c; updateSolidBrush(); }
    void setGradientColors(const QColor& c1, const QColor& c2) { gColor1 = c1; gColor2 = c2; updateGradientBrush(); }
    void setGradientColor1(const QColor& c) { gColor1 = c; updateGradientBrush(); }
    void setGradientColor2(const QColor& c) { gColor2 = c; updateGradientBrush(); }

    virtual void serialize(QDataStream& out);

protected:
    void updateSolidBrush();
    void updateGradientBrush();

    virtual void lineWidthChanged() {}
    QRectF boundingRect() const { return QRectF(-50, - 50, 100, 100); }

public:
    static qreal getDefLineWidth() { return defLineWidth; }
    static const QColor& getDefLineColor() { return defLineColor; }
    static FillStyle getDefFillStyle() { return defFillStyle; }
    static const QColor& getDefSolidColor() { return defSolidColor; }
    static const QColor& getDefGradientColor1() { return defGColor1; }
    static const QColor& getDefGradientColor2() { return defGColor2; }
    static const void getDefGradientColors(QColor& c1, QColor c2) { c1 = defGColor1; c2 = defGColor2; }

    static qreal setDefLineWidth(qreal w);
    static void setDefLineColor(const QColor& c) { defLineColor = c; }
    static void setDefFillStyle(FillStyle fs) { defFillStyle = fs; }
    static void setDefSolidColor(const QColor& c) { defSolidColor = c; }
    static void setDefGradientColors(const QColor& c1, const QColor& c2) { defGColor1 = c1; defGColor2 = c2; }
    static void setDefGradientColor1(const QColor& c) { defGColor1 = c; }
    static void setDefGradientColor2(const QColor& c) { defGColor2 = c; }

private:
    qreal lineWidth;        // толщина линии контура
    QColor lineColor;       // цвет линии контура
    FillStyle fillStyle;    // стиль заливки
    QColor solidColor;      // цвет сплошной заливки
    QColor& gColor1 = solidColor;         // 2 цвета для градиентной заливки
    QColor gColor2;

    QPen curPen;
    QBrush* curBrush;
    QBrush solidBrush;
    QBrush gradientBrush;

    static qreal defLineWidth;
    static QColor defLineColor;
    static FillStyle defFillStyle;
    static QColor defSolidColor;
    static QColor& defGColor1;
    static QColor defGColor2;
};

#endif // BASESHAPE_H
