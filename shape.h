#ifndef SHAPE_H
#define SHAPE_H

#include <QGradient>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsPathItem>
#include <QPainterPath>
#include <QPainter>
#include <QPen>
#include <QDataStream>

#include "baseshape.h"


class NodesShape: public BaseShape
{
public:
    NodesShape();
    NodesShape(QDataStream& in);

    QList<int> nodesAroundPoint(const QPointF& pt) const;
    bool nodeCaptured() const { return capturedNode != -1; }
    virtual bool changeNode(int i, const QPointF& pt) = 0;
    virtual bool addNode(const QPointF& pt) = 0;
    void changeCapturedNode(const QPointF& pt) { changeNode(capturedNode, pt); }
    void clearCapturedNode() { capturedNode = -1; }

    qreal getMinSideLen() { return nodeRadius*3; }
    virtual void serialize(QDataStream& out);

protected:
    void updatePaths(); // +

    bool pointInNode(const QPointF& pt, int i) const;

    virtual void pathsChanged() {}
    void lineWidthChanged();    // +   

    virtual QRectF boundingRect() const;    // +
    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

protected:
    QList<QPointF> nodes;
    QPainterPath outlinePath;
    QPainterPath nodesPath;

    qreal nodeRadius;
    QPen nodesPen;
    QBrush nodesBrush;

    int capturedNode;

    void mousePressEvent(QGraphicsSceneMouseEvent * event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent * event);
    void mouseMoveEvent(QGraphicsSceneMouseEvent * event);

};



#endif // SHAPE_H
