#ifndef MYGRAPHICSVIEW_H
#define MYGRAPHICSVIEW_H

#include <QGraphicsView>
#include <QGraphicsScene>
#include <QGroupBox>
#include <shape.h>

// режимы работы с фигурами сцены
enum ActionMode { AMView, AMCreation, AMDeletion, AMChange, AMRotation };

// текущий режим
extern ActionMode curActionMode;

// функция для создания фигуры по умолчанию
typedef NodesShape* (*ShapeCreatorFunc)(const QPointF&, const QPointF&);

// текущая функция создания фигуры
extern ShapeCreatorFunc shapeCreate;


class MyGraphicsView : public QGraphicsView
{
public:
    MyGraphicsView();

    void moveWorkArea(int dx, int dy);
    void moveWorkArea(const QPoint& pt);

    QGroupBox* box1ForDisable;
    QGroupBox* box2ForDisable;

protected:
    void resizeEvent(QResizeEvent * event);

    virtual void mousePressEvent(QMouseEvent * event);
    virtual void mouseMoveEvent(QMouseEvent *event);
    virtual void mouseReleaseEvent(QMouseEvent * event);
    virtual void mouseDoubleClickEvent(QMouseEvent * event);
    virtual void keyPressEvent(QKeyEvent * event);

    QPoint pressPos;
    QPoint prevMousePos;

    bool shapeCaptured;
    NodesShape* capturedShape;
    bool deletedSingleItem;
    qreal curRotation;
};

#endif // MYGRAPHICSVIEW_H
