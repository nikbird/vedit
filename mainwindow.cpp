#include "mainwindow.h"
#include <QColorDialog>
#include <QFileDialog>
#include <QFile>
#include <QMessageBox>
#include "ui_mainwindow.h"


NodesShape *createRectShape(const QPointF& pt1, const QPointF& pt2)
{
    return new RectangleShape(pt1, pt2);
}


NodesShape *createPolyShape(const QPointF &pt1, const QPointF &pt2)
{
    return new PolyShape(pt1, pt2);
}


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow), gView(new MyGraphicsView)
{
    ui->setupUi(this);
    resize(800, 600);

    curActionMode = AMView;
    shapeCreate = &createRectShape;

    gView->box1ForDisable = ui->gboxOutline;
    gView->box2ForDisable = ui->gboxFilling;
    ui->horizontalLayout->addWidget(gView);

    ui->sboxLineWidth->setMinimum(BaseShape::minLineWidth);
    QPalette palette;
    palette.setColor(QPalette::Window, BaseShape::getDefLineColor());
    ui->frameLineColor->setPalette(palette);
    palette.setColor(QPalette::Window, BaseShape::getDefGradientColor1());
    ui->frameColor1->setPalette(palette);
    palette.setColor(QPalette::Window, BaseShape::getDefGradientColor2());
    ui->frameColor2->setPalette(palette);

    connect(ui->actionSaveScene, SIGNAL(triggered(bool)), this, SLOT(saveSceneToFile()));
    connect(ui->actionLoadScene, SIGNAL(triggered(bool)), this, SLOT(loadSceneFromFile()));
    connect(ui->actionClearScene, SIGNAL(triggered(bool)), this, SLOT(clearScene()));

    connect(ui->btnShapeRectangle, SIGNAL(toggled(bool)), this, SLOT(btnShapeRectToggled()));
    connect(ui->btnShapePoly, SIGNAL(toggled(bool)), this, SLOT(btnShapePolyToggled()));

    connect(ui->btnUseLineParams, SIGNAL(clicked(bool)), this, SLOT(useLineParams()));
    connect(ui->sboxLineWidth, SIGNAL(valueChanged(int)), this, SLOT(sboxLineWidthChanged(int)));

    connect(ui->btnUseFilling, SIGNAL(clicked(bool)), this, SLOT(useFilling()));
    connect(ui->btnNoFill, SIGNAL(toggled(bool)), this, SLOT(btnNoFillChecked()));
    connect(ui->btnSolidFill, SIGNAL(toggled(bool)), this, SLOT(btnSolidFillChecked()));
    connect(ui->btnGradientFill, SIGNAL(toggled(bool)), this, SLOT(btnGradientFillChecked()));

    connect(ui->btnSetAMView, SIGNAL(toggled(bool)), this, SLOT(btnAMViewToggled()));
    connect(ui->btnSetAMCreation, SIGNAL(toggled(bool)), this, SLOT(btnAMCreationToggled()));
    connect(ui->btnSetAMDeletion, SIGNAL(toggled(bool)), this, SLOT(btnAMDeletionToggled()));
    connect(ui->btnSetAMChange, SIGNAL(toggled(bool)), this, SLOT(btnAMChangeToggled()));
    connect(ui->btnSetAMRotation, SIGNAL(toggled(bool)), this, SLOT(btnAMRotationToggled()));

    connect(ui->btnSetLineColor, SIGNAL(clicked(bool)), this, SLOT(setLineColor()));
    connect(ui->btnSetColor1, SIGNAL(clicked(bool)), this, SLOT(setColor1()));
    connect(ui->btnSetColor2, SIGNAL(clicked(bool)), this, SLOT(setColor2()));
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::saveSceneToFile()
{
    QString fileName = QFileDialog::getSaveFileName(this, QString(), QString(), "Scene File (*.scene)");
    if (fileName.isEmpty())
        return;

    try {
        QFile file(fileName);
        file.open(QIODevice::WriteOnly);
        QDataStream out(&file);

        // прямоугольник сцены
        out << gView->sceneRect();

        // теперь последовательно все объекты сцены в обратном порядке
        auto items = gView->scene()->items();
        for(int i = items.count() - 1; i >= 0; i--) {
            out << QString(typeid(*(items[i])).name());
            static_cast<NodesShape*>(items[i])->serialize(out);
            out << items[i]->pos();
        }
        file.close();
    }
    catch(...) {
        QMessageBox::warning(this, "Внимание", "Ошибка при работе с файлом");
    }

}

void MainWindow::loadSceneFromFile()
{
    QString fileName = QFileDialog::getOpenFileName(this, QString(), QString(), "Scene File (*.scene)");
    if (fileName.isEmpty())
        return;
    gView->scene()->clear();

    try {
        QFile file(fileName);
        file.open(QIODevice::ReadOnly);

        QDataStream in(&file);
        QRectF rect;
        in >> rect;
        rect.setSize(size());
        gView->setSceneRect(rect);

        QString shapeName;
        NodesShape* curShape;
        QPointF pos;

        while( ! in.atEnd() ) {
            in >> shapeName;
            if (shapeName == typeid(RectangleShape).name()) {
                curShape = new RectangleShape(in);
            }
            else if (shapeName == typeid(PolyShape).name()) {
                curShape = new PolyShape(in);
            }
            else {
                throw std::exception("Unknown shape or general error");
            }
            in >> pos;
            curShape->setPos(pos);
            gView->scene()->addItem(curShape);
        }

        file.close();
    }
    catch(...) {
        QMessageBox::warning(this, "Внимание", "Ошибка при чтении файла");
    }
}

void MainWindow::clearScene()
{
    gView->scene()->clear();
}

void MainWindow::btnShapeRectToggled()
{
    if (ui->btnShapeRectangle->isChecked()) {
        shapeCreate = &createRectShape;
    }
}

void MainWindow::btnShapePolyToggled()
{
    if (ui->btnShapePoly->isChecked()) {
        shapeCreate = &createPolyShape;
    }
}

void MainWindow::useLineParams()
{
    if (curActionMode == AMChange) {
        auto items = gView->scene()->selectedItems();
        if (items.isEmpty())
            return;
        for(int i=0; i < items.count(); i++) {
            auto item = static_cast<NodesShape*>(items[i]);
            item->setLineWidth(BaseShape::getDefLineWidth());
            item->setLineColor(BaseShape::getDefLineColor());
            item->update();
        }
    }
}


void MainWindow::sboxLineWidthChanged(int w)
{
    BaseShape::setDefLineWidth(w);
}


void MainWindow::useFilling()
{
    if (curActionMode == AMChange) {
        auto items = gView->scene()->selectedItems();
        if (items.isEmpty())
            return;
        for(int i=0; i < items.count(); i++) {
            auto item = static_cast<NodesShape*>(items[i]);
            item->setFillStyle(BaseShape::getDefFillStyle());
            item->setGradientColors(BaseShape::getDefGradientColor1(), BaseShape::getDefGradientColor2());
            item->setSolidColor(BaseShape::getDefSolidColor());
            item->update();
        }
    }
}


void MainWindow::btnNoFillChecked()
{
    if (ui->btnNoFill->isChecked())
        BaseShape::setDefFillStyle(BaseShape::FSNoFill);
}


void MainWindow::btnSolidFillChecked()
{
    if(ui->btnSolidFill->isChecked())
        BaseShape::setDefFillStyle(BaseShape::FSSolid);
}


void MainWindow::btnGradientFillChecked()
{
    if(ui->btnGradientFill->isChecked())
        BaseShape::setDefFillStyle(BaseShape::FSGradient);
}


void MainWindow::btnAMViewToggled()
{
    if (ui->btnSetAMView->isChecked()) {
        curActionMode = AMView;
        gView->scene()->clearSelection();
    }
}

void MainWindow::btnAMCreationToggled()
{
    if (ui->btnSetAMCreation->isChecked()) {
        curActionMode = AMCreation;
        gView->scene()->clearSelection();
    }
}


void MainWindow::btnAMDeletionToggled()
{
    if (ui->btnSetAMDeletion->isChecked()) {
        curActionMode = AMDeletion;
        gView->scene()->clearSelection();
    }
}

void MainWindow::btnAMChangeToggled()
{
    if (ui->btnSetAMChange->isChecked()) {
        curActionMode = AMChange;
        gView->scene()->clearSelection();
    }
    else {
        ui->gboxOutline->setEnabled(true);
        ui->gboxFilling->setEnabled(true);
    }
}

void MainWindow::btnAMRotationToggled()
{
    if (ui->btnSetAMRotation->isChecked()) {
        curActionMode = AMRotation;
        gView->scene()->clearSelection();
    }
}

void MainWindow::setLineColor()
{
    QColor c = QColorDialog::getColor();
    if (c.isValid()) {
        BaseShape::setDefLineColor(c);
        QPalette palette;
        palette.setColor(QPalette::Window, c);
        ui->frameLineColor->setPalette(palette);
    }
}

void MainWindow::setColor1()
{
    QColor c = QColorDialog::getColor();
    if (c.isValid()) {
        BaseShape::setDefGradientColor1(c);
        QPalette palette;
        palette.setColor(QPalette::Window, c);
        ui->frameColor1->setPalette(palette);
    }
}

void MainWindow::setColor2()
{
    QColor c = QColorDialog::getColor();
    if (c.isValid()) {
        BaseShape::setDefGradientColor2(c);
        QPalette palette;
        palette.setColor(QPalette::Window, c);
        ui->frameColor2->setPalette(palette);
    }
}
