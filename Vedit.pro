#-------------------------------------------------
#
# Project created by QtCreator 2016-02-21T09:59:11
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Vedit
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    shape.cpp \
    rectangle.cpp \
    polyline.cpp \
    baseshape.cpp \
    mygraphicsview.cpp

HEADERS  += mainwindow.h \
    shape.h \
    rectangle.h \
    polyline.h \
    baseshape.h \
    mygraphicsview.h

FORMS    += mainwindow.ui
