#include "polyline.h"
#include "rectangle.h"


PolyShape::PolyShape(const QPointF &pt1, const QPointF &pt2)
{
    setPos(pt1);
    nodes.reserve(2);
    nodes.push_back(QPointF(0,0));
    nodes.push_back(mapFromScene(pt2));
    updatePaths();
    capturedNode = 1;
}


PolyShape::PolyShape(QDataStream &in): NodesShape(in)
{
    pathsChanged();
}


bool PolyShape::changeNode(int i, const QPointF &pt)
{
    if (i < 0 || i >= nodes.count())
        return false;

    nodes[i] = mapFromScene(pt);
    updatePaths();
    return true;
}

bool PolyShape::addNode(const QPointF &pt)
{
    nodes.append(mapFromScene(pt));
    capturedNode = nodes.count() - 1;
    return true;
}


void PolyShape::pathsChanged()
{
    updateSegments();
    updateGradientBrush();
}


void PolyShape::lineWidthChanged()
{
    NodesShape::lineWidthChanged();
    updateSegments();
}

void PolyShape::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    painter->setBrush(QBrush(Qt::NoBrush));
    painter->setPen(getPen());
    painter->drawPath(outlinePath);
    if (isSelected()) {
        painter->setBrush(nodesBrush);
        painter->setPen(nodesPen);
        painter->drawPath(nodesPath);
    }
    option, widget;
}


void PolyShape::updateSegments()
{
    segments.clear();
    segPath = QPainterPath();
    segPath.setFillRule(Qt::WindingFill);
    for(int i = 1; i < nodes.count(); i++) {
        segments.push_back(QPainterPath());
        addSegment(segments[i - 1], i);
        segPath.addPath(segments[i - 1]);
    }
}

// endIndex = [1, nodes.count() - 1]
void PolyShape::addSegment(QPainterPath &path, int endIndex)
{
    QPointF vecA = nodes[endIndex] - nodes[endIndex-1]; //pt2 - pt1;
    QPointF vecB;

    if (vecA.x() == 0.0)
        vecB = {nodeRadius, 0.0};
    else {
        vecB = {-vecA.y()/vecA.x(), 1.0};
        vecB *= nodeRadius / sqrt(pow(vecB.x(), 2) + pow(vecB.y(), 2));
    }

    path.moveTo(nodes[endIndex-1] + vecB);
    path.lineTo(nodes[endIndex] + vecB);
    path.lineTo(nodes[endIndex] - vecB);
    path.lineTo(nodes[endIndex-1] - vecB);
    path.closeSubpath();
}


void PolyShape::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    if (capturedNode != -1) {
        if (nodes.count() > 2) {
            QList<int> nearNodes = nodesAroundPoint(event->pos());
            if ((nearNodes.count() > 1) && (qAbs(nearNodes[0] - nearNodes[1]) == 1)) {
                nodes.removeAt(capturedNode);
                updatePaths();
            }
        }
    }
    NodesShape::mouseReleaseEvent(event);
}

void PolyShape::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event)
{
    if (! nodesPath.contains(event->pos())) {
        for(int i = 1; i < nodes.count(); i++) {
            if (segments[i - 1].contains(event->pos())) {
                nodes.insert(i, event->pos());
                updatePaths();
                break;
            }
        }
    }
}



