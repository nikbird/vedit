#include "baseshape.h"


qreal BaseShape::defLineWidth = BaseShape::minLineWidth;
QColor BaseShape::defLineColor(Qt::black);
BaseShape::FillStyle BaseShape::defFillStyle = BaseShape::FillStyle::FSNoFill;
QColor BaseShape::defSolidColor(Qt::red);
QColor& BaseShape::defGColor1 = BaseShape::defSolidColor;
QColor BaseShape::defGColor2(Qt::white);


BaseShape::BaseShape(qreal w, const QColor &lcol, FillStyle fs, const QColor &c1, const QColor &c2):
    lineColor(lcol), solidColor(c1), gColor2(c2)
{
    setLineWidth(w);
    setFillStyle(fs);
    updateSolidBrush();
}

BaseShape::BaseShape(QDataStream &in)
{
    in >> lineWidth >> lineColor >> (qint32&)fillStyle >> solidColor >> gColor2;
    curPen.setColor(lineColor);
    setLineWidth(lineWidth);
    setFillStyle(fillStyle);
    updateSolidBrush();
}


BaseShape::BaseShape(): lineColor(defLineColor), solidColor(defSolidColor), gColor1(defGColor1), gColor2(defGColor2),
    curPen(QPen(lineColor))
{
    setLineWidth(defLineWidth);
    setFillStyle(defFillStyle);
    updateSolidBrush();
}


qreal BaseShape::setLineWidth(qreal w)
{
    lineWidth = (w < minLineWidth) ? minLineWidth : w;
    curPen.setWidthF(lineWidth);
    lineWidthChanged();
    return lineWidth;
}


void BaseShape::setFillStyle(BaseShape::FillStyle fs)
{
    switch (fs) {
    case FSNoFill:
        solidBrush.setStyle(Qt::NoBrush);
        curBrush = &solidBrush;
        break;
    case FSSolid:
        solidBrush.setStyle(Qt::SolidPattern);
        curBrush = &solidBrush;
        break;
    case FSGradient:
        curBrush = &gradientBrush;
        break;
    default:
        solidBrush.setStyle(Qt::NoBrush);
        curBrush = &solidBrush;
        fs = FSNoFill;
        break;
    }

    fillStyle = fs;
}

void BaseShape::serialize(QDataStream &out)
{
    /*
    qreal lineWidth;        // толщина линии контура
    QColor lineColor;       // цвет линии контура
    FillStyle fillStyle;    // стиль заливки
    QColor solidColor;      // цвет сплошной заливки
    QColor& gColor1 = solidColor;         // 2 цвета для градиентной заливки
    QColor gColor2;
     */
    out << lineWidth << lineColor << (qint32)fillStyle << solidColor << gColor2;
}


qreal BaseShape::setDefLineWidth(qreal w)
{
    defLineWidth = (w < minLineWidth) ? minLineWidth : w;
    return defLineWidth;
}


void BaseShape::updateSolidBrush()
{
    solidBrush.setColor(solidColor);
}


void BaseShape::updateGradientBrush()
{
    QLinearGradient gradient(boundingRect().topLeft(), boundingRect().bottomRight());
    gradient.setColorAt(0.0, gColor1);
    gradient.setColorAt(1.0, gColor2);
    gradientBrush = QBrush(gradient);
}

